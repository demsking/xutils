export default {
  cyan: (str: string | number) => `${str}`,
  red: (str: string | number) => `${str}`,
  yellow: (str: string | number) => `${str}`,
  green: (str: string | number) => `${str}`,
};
